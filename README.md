# Basic Mathematics

S. Lang, _Basic Mathematics_. New York, NY, USA: Springer, 1988.

- [Algebra](#algebra)
    - [Numbers](#numbers)
        - [The integers](#the-integers)
        - [Rules for addition](#rules-for-addition)
        - [Rules for multiplication](#rules-for-multiplication)
        - [Even and odd integers; divisibility](#even-and-odd-integers-divisibility)
        - [Rational numbers](#rational-numbers)
        - [Multiplicative inverses]
    - [Linear Equations](#linear-equations)
        - [Equations in two unknowns]
        - [Equations in three unknowns]
    - [Real Numbers](#real-numbers)
        - [Addition and multiplication]
        - [Real numbers: positivity]
        - [Power and roots]
        - [Inequalities]
    - [Quadratic Equations](#quadratic-equations)

## Algebra
### Numbers
#### The Integers
<table>
  <tr>
    <td>positive integers</td>
    <td>1,2,3,4,...</td>
  </tr>
  <tr>
    <td>negative integers</td>
    <td>-1,-2,-3,-4,...</td>
  </tr>
  <tr>
    <td>natural numbers</td>
    <td>positive integers with zero (0, 2, 123.521)</td>
  </tr>
  <tr>
    <td>integers</td>
    <td>positive integers, negative integers, and zero (-9, 0, 10, -5)</td>
  </tr>
</table>

__N1.__ $`0+a=a+0=a \quad \text{(for any integer a)}`$

__N2.__ $`a+(-a)=0 \enspace \text{and also} \enspace -a+a=0`$
```math
\begin{aligned}
3+(-3)=0 \enspace &\text{and} \enspace -3+3=0\\
5+(-5)=0 \enspace &\text{and} \enspace -5+5=0\\
3=-(-3) \enspace &\text{or} \enspace 5=-(-5)
\end{aligned}
```
we call $`-a`$ the __additive inverse__ of $`a`$\
the sum and product of integers are also integers
#### Rules for Addition
if $`a,b`$ are integers then $`a+b=b+a`$
__Commutativity.__ if $`a,b`$ are integers then $`a+b=b+a`$
```math
\begin{aligned}
3+5=5+3=8\\
-2+5=3=5+(-2)
\end{aligned}
```
__Associativity.__ if $`a,b,c`$ are integers then $`(a+b)+c)=a+(b+c)`$
it is unnecessary to use parentheses in a simple context
```math
\begin{aligned}
(3+5)+9&=8+9=17\\
3+(5+9)&=3+14=17\\
3+5+9&=17
\end{aligned}
```
associativity also holds with negative numbers
```math
\begin{aligned}
(-2+5)+4&=3+4=7\\
-2+(5+4)&=-2+9=7
\end{aligned}
```
```math
\begin{aligned}
(2+(-5))+(-3)&=-3+(-3)=-6\\
2+(-5+(-3))&=2+(-8)=-6
\end{aligned}
```

__N3.__ if $`a+b=0`$, then $`b=-a`$ and $`a=-b`$
```math
\begin{aligned}
a+b&=0\\
-a+a+b&=-a+0 = -a\\
-a+a+b&=b+0=b\\
b&=-a\\
\end{aligned}
```
```math
\begin{aligned}
a+b&=0\\
a+b-b&=0+(-b) = -b\\
a+b-b&=0+a=a\\
a&=-b\\
\end{aligned}
```
```math
\begin{aligned}
-b&=-(-a)=a\\
\end{aligned}
```

---
a sum involving three terms may be written in many ways
```math
\begin{aligned}
(a-b)+c &=(a+(-b))+c\\
        &=a+(-b+c)& \quad \text{by associativity}\\
        &=a+(c-b)& \quad \text{by commutativity}\\
        &=(a+c)-b& \quad \text{by associativity}\\
\end{aligned}
```

__N4.__ for any integer $`a`$ we have $`a=-(-a)`$
```math
\begin{aligned}
a+(-a)=0\\
\end{aligned}
```
if $`a`$ is positive, then $`-a`$ is negative\
if $`a`$ is negative, then $`-a`$ is positive
```math
\begin{aligned}
-3=-(-(-3))\\
-3=-(-3)=-(-(-(-3)))
\end{aligned}
```

__N5.__ for any integer $`a, b`$ we have $`-(a+b)=-a+(-b)`$
if $`a,b`$ are integers, then $`a=-b`$ and $`b=-a`$ mean that $`a+b=0`$. to prove our assertion, we must show that $`(a+b)+(-a-b)=0`$
```math
\begin{aligned}
(a+b)+(-a-b)&=a+b-a-b \quad \text{by associativity}\\
            &=a-a+b-b \quad \text{by commutativity}\\
            &=0+0\\
            &=0
\end{aligned}
```

if $`a, b`$ are positive integers, then $`a + b`$ is also a positive integer\
if $`a, b`$ are negative integers, then $`a + b`$ is negative

we can write $`a = -n`$, $`b = -m`$, where $`m, n`$ are positive
```math
\begin{aligned}
a+b=-n-m=-(n+m)
\end{aligned}
```
which shows that $`a +b`$ is negative, because $`n+m`$ is positive

__Exercises__
Justify each step, using commutativity and associativity in proving the following identities.

__1.__ $`(a+b)+(c+d)=(a+d)+(b+c)`$
```math
\begin{aligned}
&=(a+d)+(b+c)\\
&=a+(d+(b+c))\\
&=a+((d+b)+c)\\
&=a+((b+d)+c)\\
&=a+(b+(d+c)\\
&=a+(b+(c+d)\\
&=(a+b)+(c+d)
\end{aligned}
```
__2.__ $`(a+b)+(c+d)=(a+c)+(b+d)`$
```math
\begin{aligned}
&=(a+c)+(b+d)\\
&=a+(c+(b+d))\\
&=a+((c+b)+d)\\
&=a+((b+c)+d)\\
&=a+(b+(c+d))\\
&=(a+b)+(c+d))
\end{aligned}
```
__3.__ $`(a-b)+(c-d)=(a+c)+(-b-d)`$
```math
\begin{aligned}
&=(a+c)+(-b-d)\\
&=a+(c+(-b-d))\\
&=a+((c+(-b))-d)\\
&=a+((-b+c)-d)\\
&=a+((-b)+(c-d))\\
&=(a+(-b))+(c-d)\\
&=(a-b)+(c-d)
\end{aligned}
```
__4.__ $`(a-b)+(c-d)=(a+c)-(b+d)`$
```math
\begin{aligned}
&=(a+c)-(b+d)\\
&=(a+c)-b+(-d)\\
&=((a+c)-b)+(-d)\\
&=(a+(c-b))+(-d)\\
&=(a+((-b)+c))+(-d)\\
&=((a+(-b))+c)+(-d)\\
&=(a+(-b))+(c+(-d))\\
&=(a-b)+(c-d)
\end{aligned}
```
__5.__ $`(a-b)+(c-d)=(a-d)+(c-b)`$
```math
\begin{aligned}
&=(a-d)+(c-b)\\
&=(a-d)+(-b+c)\\
&=(a+(-d))+(-b+c)\\
&=a+((-d)+(-b+c))\\
&=a+(((-d)+(-b))+c)\\
&=a+(((-b)+(-d))+c)\\
&=a+((-b)+(-d+c))\\
&=a+((-b)+(c+(-d)))\\
&=(a+(-b))+(c+(-d))\\
&=(a-b)+(c-d)
\end{aligned}
```
__6.__ $`(a-b)+(c-d)=-(b+d)+(a+c)`$
```math
\begin{aligned}
&=-(b+d)+(a+c)\\
&=(-b+(-d))+(a+c)\\
&=-b+((-d)+(a+c))\\
&=-b+((a+c)+(-d))\\
&=-b+(a+(c+(-d)))\\
&=(-b+a)+(c+(-d))\\
&=(a+(-b))+(c+(-d))\\
&=(a-b)+(c-d)
\end{aligned}
```
__7.__ $`(a-b)+(c-d)=-(b+d)-(-a-c)`$
```math
\begin{aligned}
&=-(b+d)-(-a-c)\\
&=(-b+(-d))-(-a-c)\\
&=(-b+(-d))+(a+c)\\
&=(a+c)+(-b+(-d))\\
&=a+(c+(-b+(-d)))\\
&=a+((c+(-b))+(-d))\\
&=a+(((-b)+c)+(-d))\\
&=a+((-b)+(c+(-d)))\\
&=(a+(-b))+(c+(-d))\\
&=(a-b)+(c-d)
\end{aligned}
```
__8.__ $`((x+y)+z)+w=(x+z)+(y+w)`$
```math
\begin{aligned}
&=(x+z)+(y+w)\\
&=((x+z)+y)+w\\
&=(x+(z+y))+w\\
&=(x+(y+z))+w\\
&=((x+y)+z)+w
\end{aligned}
```
__9.__ $`(x-y)-(z-w)=(x+w)-y-z`$
```math
\begin{aligned}
&=(x+w)-y-z\\
&=((x+w)-y)-z\\
&=(x+(w+(-y)))-z\\
&=(x+(-y+w))-z\\
&=((x+(-y))+w)-z\\
&=(x+(-y))+(w+(-z))\\
&=(x+(-y))+(-z+w)\\
&=(x+(-y))+(-z-(-w))\\
&=(x+(-y))-(z-w)\\
&=(x-y)-(z-w)
\end{aligned}
```
__10.__ $`(x-y)-(z-w)=(x-z)+(w-y)`$
```math
\begin{aligned}
&=(x-z)+(w-y)\\
&=(x-z)+(-y+w)\\
&=(x+(-z))+(-y+w)\\
&=((x+(-z))+(-y))+w\\
&=(x+(-z+(-y)))+w\\
&=(x+(-y+(-z)))+w\\
&=((x+(-y))+(-z))+w\\
&=(x+(-y))+(-z+w)\\
&=(x+(-y))+(-z-(-w))\\
&=(x-y)-(z-w)
\end{aligned}
```
__11.__ Show that $`-(a+b+c)=-a+(-b)+(-c)`$
```math
\text{we must show that} \space (a+b+c)+(-a)+(-b)+(-c)=0
```
```math
\begin{aligned}
&=(a+b+c)+(-a-b-c)\\
&=((a+b+c)+(-a))-b-c\\
&=(a+(b+c+(-a)))-b-c\\
&=(a+(b+(-a)+c))-b-c\\
&=(a+((-a)+b+c))-b-c\\
&=(a+(-a)+(b+c)-b)-c\\
&=(a+(-a)+b+(c+(-b)))-c\\
&=(a+(-a)+b+((-b)+c))-c\\
&=(a+(-a)+b+((-b)+c)-c)\\
&=(a+(-a)+b+((-b)+c-c))\\
&=(a+(-a)+b+(-b)+(c+(-c)))\\
&=(a+(-a))+(b+(-b))+(c+(-c))\\
&=(a-a)+(b-b)+(c-c)\\
&=0+0+0=0
\end{aligned}
```
__12.__ Show that $`-(a-b-c)=-a+b+c`$\
```math
\text{we must show that} \space (a-b-c)+(-a)+b+c=0
```
```math
\begin{aligned}
&=(a-b-c)+(-a+b+c)\\
&=((a-b-c)+(-a))+b+c\\
&=(a-b+(-c+(-a)))+b+c\\
&=(a-b+(-a+(-c)))+b+c\\
&=(a+(-b+(-a)))+(-c)+b+c\\
&=(a+(-a+(-b)))+(-c)+b+c\\
&=(a+(-a)+(-b+(-c)))+b+c\\
&=(a+(-a)+(-b)+(-c+b))+c\\
&=(a+(-a)+(-b)+(b+(-c)))+c\\
&=(a+(-a)+(-b)+b+(-c+c))\\
&=(a+(-a)+(-b)+b+(c+(-c)))\\
&=(a+(-a)+(-b)+(b+c))+(-c)\\
&=(a+(-a)+(-b+b))+c+(-c)\\
&=(a+(-a)+(b+(-b))+c+(-c)\\
&=(a+(-a))+(b+(-b))+(c+(-c))\\
&=(a-a)+(b-b)+(c-c)\\
&=0+0+0=0
\end{aligned}
```
__13.__ Show that $`-(a-b)=b-a`$
```math
\text{we must show that} \space (a-b)+b-a=0
```
```math
\begin{aligned}
&=(a-b)+(b-a)\\
&=(a-b)+(-a+b)\\
&=((a-b)+(-a))+b\\
&=(a+(-b+(-a))+b\\
&=(a+(-a+(-b))+b\\
&=((a+(-a))+(-b))+b\\
&=(a+(-a))+((-b)+b)\\
&=(a-a)+(-b+b)\\
&=0+0=0
\end{aligned}
```
Solve for $`x`$ in the following equations.\
__14.__ $`-2+x=4`$
```math
\begin{aligned}
x-2&=4\\
x+(2-2)&=2+4\\
x&=6
\end{aligned}
```
__15.__ $`2-x=5`$
```math
\begin{aligned}
2-x&=5\\
(2-2)-x&=5-2\\
\frac{-x}{-1}&=\frac{3}{-1}\\
x&=-3
\end{aligned}
```
__16.__ $`x-3=7`$
```math
\begin{aligned}
x-3&=7\\
x+(3-3)&=3+7\\
x&=10
\end{aligned}
```
__17.__ $`-x+4=5`$
```math
\begin{aligned}
4-x&=5\\
(4-4)-x&=5-4\\
\frac{-x}{-1}&=\frac{1}{-1}\\
x&=-1
\end{aligned}
```
__18.__ $`4-x=8`$
```math
\begin{aligned}
4-x&=8\\
(4-4)-x&=8-4\\
\frac{-x}{-1}&=\frac{4}{-1}\\
x&=-4
\end{aligned}
```
__19.__ $`-5-x=-2`$
```math
\begin{aligned}
-5-x&=-2\\
(5-5)-x&=5-2\\
\frac{-x}{-1}&=\frac{3}{-1}\\
x&=-3
\end{aligned}
```
__20.__ $`-7+x=-10`$
```math
\begin{aligned}
x-7&=-10\\
x+(7-7)&=7-10\\
x&=-3
\end{aligned}
```
__21.__ $`-3+x=4`$
```math
\begin{aligned}
x-3&=4\\
x+(3-3)&=3+4\\
x&=7
\end{aligned}
```
__21.__ Prove: $`a+b=a+c`$ then $`b=c`$
```math
\text{let}\space a+b=a+c
```
```math
\begin{aligned}
a+b&=a+c\\
-a+a+b&=-a+a+c\\
(-a+a)+b&=(-a+a)+c\\
0+b&=0+c\\
b&=c\\
\end{aligned}
```
__22.__ Prove: if $`a+b=a`$ then $`b=0`$
```math
\begin{aligned}
a+b&=a\\
-a+a+b&=-a+a\\
(-a+a)+b&=(-a+a)\\
0+b&=0\\
b&=0\\
\end{aligned}
```
#### Rules for multiplication
the product of two integers is an integer

__Commutativity.__ $`\text{if} \space a,b \space \text{are integers (i.e. negative, positive or zero), then} \space ab=ba`$\
__Associativity.__ $`\text{if} \space a,b,c \space \text{are integers then} \space (ab)c)=a(bc)`$
```math
\begin{aligned}
(2a)(3b)&=2(a(3b))\\
        &=2(3a)b\\
        &=(2\cdot3)ab\\
        &=6ab
\end{aligned}
```
```math
\begin{aligned}
(2a)(3b)(5x)&=2a3(b(5x))\\
            &=2a3((b5)x)\\
            &=2a3((5b)x)\\
            &=2a(3(5))bx\\
            &=2(a(3\cdot5))bx\\
            &=2(3\cdot5)a)bx\\
            &=(2\cdot3\cdot5)abx\\
            &=30abx\\
\end{aligned}
```
__N6.__ $`\text{for any integer } a,\space 1a=a \enspace \text{and} \enspace 0a=0`$
```math
\begin{aligned}
0a+a&=0a+1a\\
&=(0+1)a\\
&=1a\\
&=a
\end{aligned}
```
```math
\begin{aligned}
0a+a&=a\\
0a+(a-a)&=(a-a)\\
0a+0&=0\\
0a&=0
\end{aligned}
```
__Distributivity.__ $`\text{if} \space a,b,c \space \text{are integers, then} \space a(b+c)=ab+ac \enspace \text{and} \enspace (b+c)a=ba+ca`$
```math
(b + c)a = a(b + c) = ab + ac = ba + ca
```
__N7.__ $`\text{for any integer } a,\space (-1)a = -a`$
```math
(-1)a+a=0
```
```math
\begin{aligned}
    &=(-1)a+a\\
    &=(-1)a+1a\\
    &=(-1+1)a\\
    &=0a\\
    &=0
\end{aligned}
```
__N8.__ $`\text{for any integer } a,b,\enspace -(ab)=(-a)b`$
```math
ab+(-a)b=0
```
```math
\begin{aligned}
    &=ab+(-a)b\\
    &=(a+(-a))b\\
    &=0b\\
    &=0\\
\end{aligned}
```
__N9.__ $`\text{for any integer } a,b,\enspace -(ab)=a(-b)`$
```math
(ab)+a(-b)=0
```
```math
\begin{aligned}
    &=(ab)+a(-b)\\
    &=(b+(-b))a\\
    &=0a\\
    &=0\\
\end{aligned}
```

the product of two minus signs gives a plus sign
```math
(-1)(-1)=1
```
```math
\begin{aligned}
(-1)(-1)&=-(1(-1))\\
        &=-(-1)\\
        &=1

\end{aligned}
```
__N10.__ $`\text{for any integer } a,b\text{ we have}\space (-a)(-b)=ab`$
```math
\begin{aligned}
(-a)(-b)&=-(a(-b))\\
        &=-(-(ab)\\
        &=ab
\end{aligned}
```
the product of a negative and a positive number is negative
```math
\begin{aligned}
(-4)\cdot7&=-(4\cdot7)\\
&=-28
\end{aligned}
```

---
```math
aa=a^2\newline
aaa=a^3\newline
aaaa=a^4
```
$`a^n`$ is the n-th power of $`a`$
```math
a^n=aa\cdots a
```
__N11.__ $`\text{if } m,n\text{ are positive integers, then}\space a^{m+n}=a^ma^n`$
```math
a^2a^3=(aa)(aaa)=a^{2+3}=aaaaa=a^5
```
```math
(4x)^2 = 4x\cdot4x = 4\cdot4xx = 16x^2
```
```math
(7x)(2x)(5x) = 7\cdot2\cdot5xxx=70x^3
```
__N12.__ $`\text{if } m,n\text{ are integers, then}\space (a^m)^n=a^{mn}`$
```math
(a^3)^4 = a^{12}
```
```math
(ab)^n = a^n·b^n
```
```math
\begin{aligned}
(ab)^n &= abab\cdots ab\\
       &= \underbrace{aa\cdots a}_{\text{n}}\underbrace{bb\cdots b}_{\text{n}}\\
       &= a^nb^n\\
\end{aligned}
```
```math
(2a^3)^5 = 2^5(a^3)^5 = 32a^{15}
```

__Formulae__
```math
(a+b)^2=a^2+2ab+b^2
```
```math
\begin{aligned}
(a+b)^2&=(a+b)(a+b)\\
    &=a(a+b)+b(a+b)\\
    &=aa+ab+ba+bb\\
    &=a^2+ab+ab+b^2\\
    &=a^2+2ab+b^2
\end{aligned}
```

```math
(a-b)^2=a^2-2ab+b^2
```
```math
\begin{aligned}
(a-b)^2&=(a-b)(a-b)\\
    &=a(a-b)-b(a-b)\\
    &=aa-ab-ba-bb\\
    &=a^2-ab-ba-b^2\\
    &=a^2-2ab-b^2
\end{aligned}
```
```math
(a+b)(a-b)=a^2-b^2
```
```math
\begin{aligned}
(a+b)(a-b)&=a(a-b)+b(a-b)\\
          &=aa-ab+ba-bb\\
          &=a^2-ab+ab-b^2\\
          &=a^2-b^2
\end{aligned}
```
__Example__
```math
(2x+1)(x-2)(x+5)
```
```math
\begin{aligned}
(2x+1)(x-2)&=2x(x-2)+1(x-2)\\
    &=2x^2-4x+x-2\\
    &=2x^2-3x-2\\
\end{aligned}
```
```math
\begin{aligned}
(2x+1)(x-2)(x+5)&=(2x^2-3x-2)(x+5)\\
    &=(2x^2-3x-2)x+(2x^2-3x-2)5\\
    &=2x^3-3x^2-2x+10x^2-15x-10\\
    &=2x^3-7x^2-17x-10\\
\end{aligned}
```
__Exercises__
__1.__ Express each of the following expressions in the form $`2^m3^na^rb^8`$, where $`m,n,r,s`$ are positive integers.
```math
\text{a)}\space 8a^2b^3(27a^4)(2^5ab)
```
```math
\begin{aligned}
&=8a^2b^3(27a^4)(2^5ab)\\
&=2^3a^2b^2(3^3a^4)(2^5ab)\\
&=(2^3\cdot2^5)3^3(a^4a^2a)(b^2b)\\
&=2^{5+3}3^3a^{4+2+1}b^{2+1}\\
&=2^83^3a^7b^4
\end{aligned}
```
```math
\text{b)}\space 16b^3a^2(6ab^4)(ab)^3
```
```math
\begin{aligned}
&=16b^3a^2(6ab^4)(ab)^3\\
&=2^4b^3a^2(2\cdot3ab^4)a^3b^3\\
&=(2^4\cdot2)3(a^3a^2a)(b^4b^3b^3)\\
&=2^{4+1}3^1a^{3+2+1}b^{4+3+3}\\
&=2^53^1a^6b^{10}\\
\end{aligned}
```
```math
\text{c)}\space 3^2(2ab)^3(16a^2b^5)(24b^2a)
```
```math
\begin{aligned}
&=3^2(2ab)^3(16a^2b^5)(24b^2a)\\
&=3^2\cdot 2^3a^3b^3(2^4a^2b^5)(2^3\cdot3b^2a)\\
&=(2^4\cdot2^3\cdot2^3)(3^2\cdot3)(a^3a^2a)(b^5b^3b^2)\\
&=2^{4+3+3}3^{2+1}a^{3+2+1}b^{5+3+2}\\
&=2^{10}3^3a^6b^{10}
\end{aligned}
```
```math
\text{d)}\space 24a^3(2ab^2)^3(3ab)^2
```
```math
\begin{aligned}
&=24a^3(2ab^2)^3(3ab)^2\\
&=2^3\cdot3a^3(2^3a^3(b^2)^3)(3^2a^2b^2)\\
&=(2^3\cdot2^3)(3^2\cdot3)(a^3a^3a^2)((b^2)^3b^2)\\
&=2^{3+3}3^{2+1}a^{3+3+2}b^{(2\cdot3)+2}\\
&=2^63^3a^8b^8\\
\end{aligned}
```
```math
\text{e)}\space (3ab)^2(27a^3b)(16ab^5)
```
```math
\begin{aligned}
&=(3ab)^2(27a^3b)(16ab^5)\\
&=(3^2a^2b^2)(3^3a^3b)(2^4ab^5)\\
&=2^4(3^2\cdot3^3)(a^3a^2a)(b^5b^2b)\\
&=2^43^{3+2}a^{3+2+1}b^{5+2+1}\\
&=2^43^5a^6b^8
\end{aligned}
```
```math
\text{f)}\space 32a^4b^5a^3b^2(6ab^3)^4
```
```math
\begin{aligned}
&=32a^4b^5a^3b^2(6ab^3)^4\\
&=2^5a^4b^5a^3b^2(2^4\cdot3^4a^4(b^3)^4)\\
&=(2^5\cdot2^4)3^4(a^4a^4a^3)((b^3)^4b^5b^2)\\
&=2^{5+4}3^4a^{4+4+3}b^{(3\cdot4)+5+2}\\
&=2^{9}3^4a^{11}b^{19}
\end{aligned}
```
__2.__ Prove:
```math
(a+b)^3=a^3+3a^2b+3ab^2+b^3
```
```math
\begin{aligned}
(a+b)^3&=(a+b)^2(a+b)\\
    &=(a^2+2ab+b^2)(a+b)\\
    &=(a^2+2ab+b^2)a+(a^2+2ab+b^2)b\\
    &=a^3+2a^2b+b^2a+a^2b+2ab^2+b^3\\
    &=a^3+3a^2b+3ab^2+b^3\\    
\end{aligned}
```
```math
(a-b)^3=a^3-3a^2b+3ab^2-b^3
```
```math
\begin{aligned}
(a-b)^3&=(a-b)^2(a-b)\\
    &=(a^2-2ab+b^2)(a-b)\\
    &=(a^2-2ab+b^2)a-(a^2-2ab+b^2)b\\
    &=a^3-2a^2b+b^2a-a^2b+2ab^2-b^3\\
    &=a^3-3a^2b+3ab^2-b^3
\end{aligned}
```
__3.__ Obtain expansions for $`(a+b)^4`$ and $`(a-b)^4`$ similar to the expansions for $`(a+b)^3`$ and $`(a-b)^3`$ of the preceding exercise.
```math
\begin{aligned}
(a+b)^4&=(a+b)^3(a+b)\\
    &=(a^3+3a^2b+3ab^2+b^3)(a+b)\\
    &=(a^3+3a^2b+3ab^2+b^3)a+(a^3+3a^2b+3ab^2+b^3)b\\
    &=a^4+3a^3b+3a^2b^2+b^3a+a^3b+3a^2b^2+3ab^3+b^4\\
    &=a^4+4a^3b+6a^2b^2+4ab^3+b^4
\end{aligned}
```
```math
\begin{aligned}
(a-b)^4&=(a-b)^3(a-b)\\
    &=(a^3-3a^2b+3ab^2-b^3)(a-b)\\
    &=(a^3-3a^2b+3ab^2-b^3)a-(a^3-3a^2b+3ab^2-b^3)b\\
    &=a^4-3a^3b+3a^2b^2-b^3a-a^3b+3a^2b^2-3ab^3+b^4\\
    &=a^4-4a^3b+6a^2b^2-4ab^3+b^4\\
\end{aligned}
```
Expand the following expressions as sums of powers of $`x`$ multiplied by integers.\
__4.__ $`(2-4x)^2`$
```math
\begin{aligned}
(2-4x)^2&=(2-4x)(2-4x)\\
    &=2(2-4x)-4x(2-4x)\\
    &=2\cdot2+2\cdot-4x+(-4x)\cdot2+(-4x)\cdot-4x\\
    &=4-8x-8x-4(-4)x^2\\
    &=16x^2-16x+4\\
\end{aligned}
```
__5.__ $`(1-2x)^2`$
```math
\begin{aligned}
(1-2x)^2&=(1-2x)(1-2x)\\
    &=1(1-2x)-2x(1-2x)\\
    &=1\cdot1+1\cdot-2x+(-2x)\cdot1+(-2x)\cdot-2x\\
    &=1-2x-2x-2(-2)x^2\\
    &=4x^2-4x+1
\end{aligned}
```
__6.__ $`(2x+5)^2`$
```math
\begin{aligned}
(2x+5)^2&=(2x+5)(2x+5)\\
    &=2x(2x+5)+5(2x+5)\\
    &=2x\cdot2x+2x\cdot5+5\cdot2x+5\cdot5\\
    &=4x^2+10x+10x+25\\
    &=4x^2+20x+25
\end{aligned}
```
__7.__ $`(x-1)^2`$
```math
\begin{aligned}
(x-1)^2&=(x-1)(x-1)\\
    &=x(x-1)-1(x-1)\\
    &=xx+x\cdot-1+(-1)\cdot x+(-1)\cdot-1\\
    &=x^2-x-x+1\\
    &=x^2-2x+1
\end{aligned}
```
__8.__ $`(x+1)(x-1)`$
```math
\begin{aligned}
(x+1)(x-1)&=x(x-1)+1(x-1)\\
    &=xx+x-1+1\cdot x+1\cdot-1\\
    &=x^2-x+x-1\\
    &=x^2-1
\end{aligned}
```
__9.__ $`(2x+1)(x+5)`$
```math
\begin{aligned}
(2x+1)(x+5)&=2x(x+5)+1(x+5)\\
    &=2xx+2x\cdot5+1\cdot x+1\cdot5\\
    &=2x^2+10x+x+5\\
    &=2x^2+11x+5
\end{aligned}
```
__10.__ $`(x^2+1)(x^2-1)`$
```math
\begin{aligned}
(x^2+1)(x^2-1)&=x^2(x^2-1)+1(x^2-1)\\
    &=x^2x^2+x^2\cdot-1+1\cdot x^2+1\cdot-1\\
    &=x^{4}-x^2+x^2-1\\
    &=x^4-1
\end{aligned}
```
__11.__ $`(1+x^3)(1-x^3)`$
```math
\begin{aligned}
(1+x^3)(1-x^3)&=1(1-x^3)+x^3(1-x^3)\\
    &=1\cdot1+1\cdot -x^3+x^3\cdot1+x^3(-x^3)\\
    &=1-x^3+x^3+-x^{6}\\
    &=-x^6+1
\end{aligned}
```
__12.__ $`(x^2+1)^2`$
```math
\begin{aligned}
(x^2+1)^2&=(x^2+1)(x^2+1)\\
    &=x^2(x^2+1)+1(x^2+1)\\
    &=x^2x^2+x^2\cdot1+1\cdot x^2+1\cdot1\\
    &=x^{4}+x^2+x^2+1\\
    &=x^4+2x^2+1
\end{aligned}
```
__13.__ $`(x^2-1)^2`$
```math
\begin{aligned}
(x^2-1)^2&=(x^2-1)(x^2-1)\\
    &=x^2(x^2-1)-1(x^2-1)\\
    &=x^2x^2+x^2\cdot-1+(-1)\cdot x^2+(-1)\cdot-1\\
    &=x^4-x^2-x^2+1\\
    &=x^4-2x^2+1
\end{aligned}
```
__14.__ $`(x^2+2)^2`$
```math
\begin{aligned}
(x^2+2)^2&=(x^2+2)(x^2+2)\\
    &=x^2(x^2+2)+2(x^2+2)\\
    &=x^2x^2+x^2\cdot2+2\cdot x^2+2\cdot2\\
    &=x^4+2x^2+2x^2+4\\
    &=x^4+4x^2+4
\end{aligned}
```
__15.__ $`(x^2-2)^2`$
```math
\begin{aligned}
(x^2-2)^2&=(x^2-2)(x^2-2)\\
    &=x^2(x^2-2)-2(x^2-2)\\
    &=x^2x^2+x^2\cdot-2+(-2)\cdot x^2+(-2)\cdot-2\\
    &=x^4-2x^2-2x^2+4\\
    &=x^4-4x^2+4
\end{aligned}
```
__16.__ $`(x^3-4)^2`$
```math
\begin{aligned}
(x^3-4)^2&=(x^3-4)(x^3-4)\\
    &=x^3x^3-4x^3-4x^3-4(-4)\\
    &=x^6-8x^3+16
\end{aligned}
```
__17.__ $`(x^3-4)(x^3+4)`$
```math
\begin{aligned}
(x^3-4)(x^3+4)&=x^3x^3+4x^3-4x^3-4\cdot4\\
    &=x^6-16\\
\end{aligned}
```
__18.__ $`(2x^2+1)(2x^2-1)`$
```math
\begin{aligned}
(2x^2+1)(2x^2-1)&=2\cdot2x^2x^2-2x^2+2x^2-1\\
    &=4x^4-1\\
\end{aligned}
```
__19.__ $`(-2+3x)(-2-3x)`$
```math
\begin{aligned}
(-2+3x)(-2-3x)&=4+6x-6x+3\cdot-3xx\\
    &=-9x^2+4\\
\end{aligned}
```
__20.__ $`(x+1)(2x+5)(x-2)`$
```math
\begin{aligned}
(x-2)(x+1)&=xx+x-2x-2\\
    &=x^2-x-2\\
    \\
(x-2)(x+1)(2x+5)&=(x^2-x-2)(2x+5)\\
        &=(x^2-x-2)2x+(x^2-x-2)5\\
        &=2x^3-2x^2-4x+5x^2-5x-10\\
        &=2x^3+3x^2-9x-10   
\end{aligned}
```
__21.__ $`(2x+1)(1-x)(3x+2)`$
```math
\begin{aligned}
(1-x)(2x+1)&=2x+1-2x^2-x\\
    &=-2x^2+x+1\\
    \\
(1-x)(2x+1)(3x+2)&=(-2x^2+x+1)(3x+2)\\
        &=-6x^3+3x^2+3x-4x^2+2x+2\\
        &=-6x^3-x^2+5x+2
\end{aligned}
```
__22.__ $`(3x-1)(2x+1)(x+4)`$
```math
\begin{aligned}
(x+4)(2x+1)&=2x^2+x+8x+4\\
    &=2x^2+9x+4\\
    \\
(x+4)(2x+1)(3x-1)&=(2x^2+9x+4)(3x-1)\\
    &=6x^3+27x^2+12x-2x^2-9x-4\\
    &=6x^3+25x^2+3x-4
\end{aligned}
```
__23.__ $`(-1-x)(-2+x)(1-2x)`$
```math
\begin{aligned}
(-1-x)(-2+x)&=2-x+2x-x^2\\
    &=-x^2+x+2\\
    \\
(-1-x)(-2+x)(1-2x)&=(-x^2+x+2)(1-2x)\\
    &=-x^2+x+2+2x^3-2x^2-4x\\
    &=2x^3-3x^2-3x+2
\end{aligned}
```
__24.__ $`(-4x+1)(2-x)(3+x)`$
```math
\begin{aligned}
(1-4x)(2-x)&=2-x-8x+4x^2\\
    &=4x^2-9x+2\\
    \\
(1-4x)(2-x)(3+x)&=(4x^2-9x+2)(3+x)\\
    &=12x^2-27x+6+4x^3-9x^2+2x\\
    &=4x^3+3x^2-25x+6
\end{aligned}
```
__25.__ $`(1-x)(1+x)(2-x)`$
```math
\begin{aligned}
(1-x)(2-x)&=2-x-2x+x^2\\
    &=x^2-3x+2\\
    \\
(1-x)(2-x)(1+x)&=(x^2-3x+2)(1+x)\\
    &=x^2-3x+2+x^3-3x^2+2x\\
    &=x^3-2x^2-x+2
\end{aligned}
```
__26.__ $`(x-1)^2(3-x)`$
```math
\begin{aligned}
(x-1)^2&=(x-1)(x-1)\\
    &=x^2-x-x+1\\
    &=x^2-2x+1\\
    \\
(x-1)^2(3-x)&=(x^2-2x+1)(3-x)\\
    &=3x^2-6x+3-x^3+2x^2-x\\
    &=-x^3+5x^2-7x+3
\end{aligned}
```
__27.__ $`(1-x)^2(2-x)`$
```math
\begin{aligned}
(1-x)^2&=(1-x)(1-x)\\
    &=1-x-x+x^2\\
    &=x^2-2x+1\\
    \\
(1-x)^2(2-x)&=(x^2-2x+1)(2-x)\\
    &=2x^2-4x+2-x^3+2x^2-x\\
    &=-x^3+4x^2-5x+2
\end{aligned}
```
__28.__ $`(1-2x)^2(3+4x)`$
```math
\begin{aligned}
(1-2x)^2&=(1-2x)(1-2x)\\
    &=1-2x-2x+4x^2\\
    &=4x^2-4x+1\\
    \\
(1-2x)^2(3+4x)&=(4x^2-4x+1)(3+4x)\\
    &=12x^2-12x+3+16x^3-16x^2+4x\\
    &=16x^3-4x^2-8x+3
\end{aligned}
```
__29.__ $`(2x+1)^2(2-3x)`$
```math
\begin{aligned}
(2x+1)^2&=(2x+1)(2x+1)\\
    &=4x^2+2x+2x+1\\
    &=4x^2+4x+1\\
    \\
(2x+1)^2(2-3x)&=(4x^2+4x+1)(2-3x)\\
    &=8x^2+8x+2-12x^3-12x^2-3x\\
    &=-12x^3-4x^2+5x+2
\end{aligned}
```
__30.__ The population of a city in 1910 was 50000, and it doubles every 10 years. What will it be (a) in 1970 (b) in 1990 (c) in 2000?
```math
2^{a}\cdot 50000 \space \text{where} \space a\text{ is a unit in decades}\newline
```
```math
\begin{aligned}
\def\pop{50000}
\text{a)}&\space 2^6\cdot\pop=2(2(2(2(2(2(\pop))))))=3200000\\
\text{b)}&\space 2^8\cdot\pop=2(2(2(2(2(2(2(2(\pop))))))))=12800000\\
\text{c)}&\space 2^9\cdot\pop=2(2(2(2(2(2(2(2(2(\pop)))))))))=25600000
\end{aligned}
```
__31.__ The population of a city in 1905 was 100000, and it doubles every 25 years. What will it be after (a) 50 years (b) 100 years (c) 150 years?
```math
2^{a}\cdot 100000 \space \text{where} \space a\text{ is a unit in quarter-centuries}
```
```math
\begin{aligned}
\def\pop{100000}
\text{a)}&\space 2^2\cdot\pop=400000\\
\text{b)}&\space 2^4\cdot\pop=1600000\\
\text{c)}&\space 2^6\cdot\pop=6400000
\end{aligned}
```
__32.__ The population of a city was 200000 in 1915, and it triples every 50 years. What will be the population (a) in the year 2215 (b) in the year 2165?
```math
3^{a}\cdot 200000 \space \text{where} \space a\text{ is a unit in half-centuries}
```
```math
\begin{aligned}
\def\pop{200000}
\text{a)}&\space 3^6\cdot\pop=3(3(3(3(3(3(\pop))))))=145800000\\
\text{b)}&\space 3^5\cdot\pop=3(3(3(3(3(3(\pop))))))=48600000
\end{aligned}
```
__32.__ The population of a city was 25000 in 1870, and it triples every 40 years. What will it be (a) in 1990 (b) in 2030?
```math
3^{a}\cdot 25000 \space \text{where} \space a\text{ is a unit in four decades}
```
```math
\begin{aligned}
\def\pop{25000}
\text{a)}&\space 3^3\cdot\pop=675000\\
\text{b)}&\space 3^4\cdot\pop=2025000
\end{aligned}
```
#### Even and odd integers; divisibility
<table>
    <tr>
        <td>odd integers</td>
        <td>1,3,5,7,9,11,13,...</td>
    </tr>
    <tr>
        <td>even integers</td>
        <td>2,4,6,8,10,12,14,...</td>
    </tr>
</table>

the odd integers go up by 2 and the even integers go up by 2.\
an even integer can be written in the form $`2n`$ for some positive integer $`n`$.
```math
\begin{aligned}
2=2\cdot1\\
4=2\cdot2\\
6=2\cdot3\\
8=2\cdot4
\end{aligned}
```
an odd integer is an integer which differs from an even integer by 1, and can be written in the form $`2m-1`$ for some positive integer $`m`$.
```math
\begin{aligned}
1=2\cdot1-1\\
3=2\cdot2-1\\
5=2\cdot3-1\\
7=2\cdot4-1\\
9=2\cdot5-1
\end{aligned}
```
or in the form $`2n+1`$ if we allow $`n`$ to be a natural number, i.e., allowing $`n=0`$.
```math
\begin{aligned}
1=2\cdot0+1\\
3=2\cdot1+1\\
5=2\cdot2+1\\
7=2\cdot3+1\\
9=2\cdot4+1
\end{aligned}
```
__Theorem 1.__ Let $`a,b`$ be positive integers.\
if $`a`$ is even and $`b`$ is even, then $`a+b`$ is even.
if $`a`$ is even and $`b`$ is odd, then $`a+b`$ is odd.
if $`a`$ is odd and $`b`$ is even, then $`a+b`$ is odd.
if $`a`$ is odd and $`b`$ is odd, then $`a+b`$ is even.

assume that $`a`$ is even and $`b`$ is odd. then we can write for some positive integer $`n`$ and some natural number $`k`$.
```math
a=2n \quad \text{and}\quad b=2k+1
```
```math
\begin{aligned}
a+b&=2n+2k+1\\
    &=2(n+k)+1\\
    &=2m+1
\end{aligned}
```
letting $`m=n+k`$ proves that $`a+b`$ is odd.

__Theorem 2.__ $`\text{let}\space a \space\text{be a positive integer. if}\space a \space\text{is even, then}\space a^2 \space \text{is even. if}\space a \space\text{is odd, then}\space a^2 \space \text{is odd.}`$

assume that $`a`$ is even. this means $`a=2n`$ for some positive integer $`n`$.
```math
a^2=2n\cdot2n=2(2n^2)=2m,
```
where $`m=2n^2`$ is a positive integer, thus $`a^2`$ is even.

next, assume that $`a`$ is odd, and write $`a=2n+1`$ for some natural number $`n`$.
```math
\begin{aligned}
a^2=(2n+1)^2&=(2n)^2+2(2n)1+1^2\\
&=4n^2+4n+1\\
&=2(2n^2+2n)+1\\
&=2k+1
\end{aligned}
```
where $`k=2n^2+2n`$ is a positive integer, thus $`a^2`$ is odd.

__Corollary.__ $`\text{let}\space a \space\text{be a positive integer. if}\space a^2 \space\text{is even, then}\space a \space \text{is even. if}\space a^2 \space\text{is odd, then}\space a \space \text{is odd.}`$

if $`a^2`$ is even, then $`a`$ cannot be odd because the square of an odd number is odd.
if $`a^2`$ is odd, then $`a`$ cannot be even because the square of an even number is even.

we can generalize the property used to define an even integer. let $`d`$ be a positive integer and let $`n`$ be an integer. we shall say that $`d`$ divides $`n`$, or that $`n`$ is divisible by $`d`$ if we can write $`n=dk`$ for some integer $`k`$.

__Exercises__
__1.__ Give the proofs for the cases of __Theorem 1__.

1.  if  $`a`$ is even and  $`b`$ is even, then  $`a+b`$ is even.\
    if $`a=2n`$, $`b=2m`$, then
```math
\begin{aligned}
a+b&=2n+2m\\
&=2(n+m)\\
&=2k \qquad\qquad \text{(letting }k=n+m\text{)}
\end{aligned}
```
2. if  $`a`$ is even and  $`b`$ is odd, then  $`a+b`$ is odd.\
    if $`a=2n`$, $`b=2m+1`$, then
```math
\begin{aligned}
a+b&=2n+2m+1\\
&=2(n+m)+1\\
&=2k+1 \qquad\qquad \text{(letting }k=n+m\text{)}
\end{aligned}
```
3. if  $`a`$ is odd and  $`b`$ is even, then  $`a+b`$ is odd.\
    if $`a=2n+1`$, $`b=2m`$, then
```math
\begin{aligned}
a+b&=2n+1+2m\\
&=2(n+m)+1\\
&=2k+1 \qquad\qquad \text{(letting }k=n+m\text{)}
\end{aligned}
```
4. if  $`a`$ is odd and  $`b`$ is odd, then  $`a+b`$ is even.\
    if $`a=2n+1`$, $`b=2m+1`$, then

```math
\begin{aligned}
a+b&=2n+1+2m+1\\
&=2(n+m)+2\\
&=2k+2 \qquad\qquad \text{(letting }k=n+m\text{)}
\end{aligned}
```
__2.__ Prove: If $`a`$ is even and $`b`$ is any positive integer, then $`ab`$ is even.

we need to consider two cases:
1. let $`a`$ be even and $`b`$ be odd. then by definition $`a=2n`$ and $`b=2m+1`$ for $`n,m`$ integers.
    ```math
    \begin{aligned}
    ab&=(2n)(2m+1)\\
    &=4nm+2n\\
    &=2(2nm+n)\\
    &=2k
    \end{aligned}
    ```
   where $`k=2nm+n`$ is an integer. therefore by definition the product $`ab`$ is even.
2. let both $`a`$ and $`b`$ be even, then $`a=2n`$ and $`b=2m`$ for $`n,m`$ integers.
    ```math
    \begin{aligned}
    ab&=(2n)(2m)\\
        &=2(2nm)\\
        &=2k
    \end{aligned}
    ```
    where $`k = 2nm`$ is an integer. Therefore the product $`ab`$ is even by definition.

__3.__ Prove: if $`a`$ is even, then $`a^3`$ is even.

let $`a`$ be even, then $`a=2n`$ for any positive integer $`n`$
```math
\begin{aligned}
a^3&=(2n)^3\\
    &=2^3n^3\\
    &=2(2^2n^3)\\
    &=2k
\end{aligned}
```
setting $`k=2^2n^3`$ gives $`a^3=2k`$, thus $`a^3`$ is even.

__4.__ Prove: if $`a`$ is odd, then $`a^3`$ is odd.

let $`a`$ be odd, then $`a=2n+1`$ for any positive integer $`n`$
```math
\begin{aligned}
a^3&=(2n+1)^3\\
    &=8n^3+12n^2+6n+1\\
    &=2(4n^3+6n^2+3n)+1\\
    &=2k+1
\end{aligned}
```
setting $`k=4n^3+6n^2+3n`$ gives $`a^3=2k+1`$, thus $`a^3`$ is odd.

__5.__ Prove: if $`n`$ is even, then $`(-1)^n=1`$

let $`n`$ be even, then $`n=2k`$ for any positive integer $`k`$
```math
\begin{aligned}
(-1)^n&=(-1)^{2k}\\
    &=((-1)^2)^k\\
    &=1^k
\end{aligned}
```
then $`1^k=1`$ for all integers, $`k`$

__6.__ Prove: if $`n`$  is odd, then $`(-1)^n=-1`$

let $`n`$ be odd, then $`n=2k+1`$ for any positive integer $`k`$
```math
\begin{aligned}
(-1)^n&=(-1)^{2k+1}\\
    &=(-1)^{2k}(-1)\\
    &=((-1)^2)^k(-1)\\
    &=1^k(-1)\\
    &=-1
\end{aligned}
```

__7.__ Prove: if $`m,n`$ are odd, then the product $`mn`$ is odd.

let both $`m`$ and $`n`$ be odd, then $`m=2k+1`$ and $`n=2q+1`$ for $`k,q`$ integers.
```math
\begin{aligned}
mn&=(2k+1)(2q+1)\\
    &=4kq+2k+2q+1\\
    &=2(2kq+k+q)+1\\
    &=2t+1\\
\end{aligned}
```
setting $`t=2kq+q+k`$ gives $`mn=2t+1`$, thus $`mn`$ is odd.

__8-15.__ Find the largest power of 2 which divides the following integers.
```math
\begin{aligned}
16&=2\cdot2\cdot2\cdot2=2^4\\
24&=2\cdot2\cdot2\cdot3=2^3\cdot3^1\\
32&=2\cdot2\cdot2\cdot2\cdot2=2^5\\
20&=2\cdot2\cdot5=2^2\cdot5^1\\
50&=2\cdot5\cdot5=2^1\cdot5^2\\
64&=2\cdot2\cdot2\cdot2\cdot2\cdot2=2^6\\
100&=2\cdot2\cdot5\cdot5=2^2\cdot5^2\\
36&=2\cdot2\cdot3\cdot3=2^2\cdot3^2\\
\end{aligned}
```
__17-23.__ Find the largest power of 3 which divides the following integers.
```math
\begin{aligned}
30&=2\cdot3\cdot5=2^1\cdot3^1\cdot5^1\\
27&=3\cdot3\cdot3=3^3\\
63&=3\cdot3\cdot7=3^2\cdot7^1\\
99&=3\cdot3\cdot11=3^2\cdot11^1\\
60&=3\cdot2\cdot2\cdot5=3^1\cdot2^2\cdot5^1\\
50& \space \text{is not divisible by}\space 3\text{.}\\
42&=2\cdot3\cdot7=2^1\cdot3^1\cdot7^1\\
45&=3\cdot3\cdot5=3^2\cdot5^1
\end{aligned}
```
__24.__ Let $`a,b`$ be integers. Define $`a\equiv b\pmod 5`$, which we read "$`a`$ is congruent to $`b`$ modulo 5", to mean that $`a-b`$ is divisible by 5. Prove: If $`a\equiv b\pmod 5 `$ and $`x\equiv y\pmod 5`$, then $`a+x\equiv b+y\pmod 5`$ and $`ax\equiv by\pmod 5`$

__25.__ Let $`d`$ be a positive integer. Let $`a,b`$ be integers. Define $`a\equiv b\pmod d`$ to mean $`a-b`$ is divisible by $`d`$. Prove that if $`a\equiv b\pmod d `$ and $`x\equiv y\pmod d`$, then $`a+x\equiv b+y\pmod d`$ and $`ax\equiv by\pmod d`$

if $`a\equiv b\pmod d`$ then by definition $`d|(a-b)`$ to mean $`d`$ divides $`(a-b)`$. therefore, $`a-b=dq`$ for some $`q`$. thus $`a=b+dq`$. conversely if $`a=b+dq`$, then $`a-b=dq`$ and so $`d|(a-b)`$ and hence $`a\equiv b\pmod d`$ then $`a=b+dq`$

1. $`a+c\equiv b+y\pmod d`$\
    write $`a=b+dq_1`$ and $`x=y+dq_2`$. adding equalities, we get
    ```math
    \begin{aligned}
    a+x&=b+y+dq_1+dq_2\\
        &=b+y+d(q_1+q_2)
    \end{aligned}
    ```
    this shows that $`a+x \equiv b+y \pmod d`$

2. $`ax\equiv by\pmod d`$\
    similarly, multiplying we get
    ```math
    \begin{aligned}
    ax&=(b+dq_1)(y+dq_2)\\
        &=by+dbq_2+dyq_1+d^2q_1q_2\\
        &=by+d(bq_2+yq_1+dq_1q_2)
    \end{aligned}
    ```
    and so $`ax \equiv by \pmod d`$

__26.__ Assume that every positive integer can be written in one of the forms $`3k`$, $`3k+1`$, $`3k+2`$ for some integer $`k`$. Show that if the square of a positive integer is divisible by $`3`$, then so is the integer.

for any positive integer a, either:
1.  suppose $`a=3k`$ then
    ```math
    \begin{aligned}
    a^2=(3k)^2=9k^3=3(3k^2)
    \end{aligned}
    ```
    $`a^2`$ is divisible by $`3`$ and $`a`$ is divisible by $`3`$.
2. suppose $`a=3k+1`$ then
    ```math
    \begin{aligned}
    a^2&=(3k+1)^2\\
        &=9k^2+6k+1\\
        &=3(3k^2+2k)+1
    \end{aligned}
    ```
    $`a^2`$ is not divisible by $`3`$ and $`a`$ is not divisible by $`3`$.
3. suppose $`a=3k+2`$ then
    ```math
    \begin{aligned}
    a^2&=(3k+2)^2\\
        &=9k^2+12k+4\\
        &=3(3k^2+4k+1)+1
    \end{aligned}
    ```
    $`a^2`$ is not divisible by $`3`$ and $`a`$ is not divisible by $`3`$.
#### Rational numbers
rational number: an ordinary fraction, that is a quotient $`\frac m n`$ where $`m,n`$ are integers and $`n \not = 0`$

__Rule for cross-multiplying.__ Let $`m,n,r,s`$ be integers and assume that $`n \not = 0`$ and $`s \not = 0`$
```math
\frac mn=\frac rs \quad \text{if and only if}\quad ms=rn
```
we make no distinction between an integer $`m`$ and the rational number $`\frac m1`$
```math
m=m/1=\frac m1
```

__Cancellation rule for fractions.__ Let $`a`$ be a non-zero integer. Let $`m,n`$ be integers, $`n \not = 0`$
```math
\frac{am}{an}=\frac mn
```
_Proof._ to test equality, we apply the rule for cross multiplying.
```math
(am)n=m(an)
```
```math
\begin{aligned}
&=(am)n\\
&=(ma)n\\
&=m(an)
\end{aligned}
```
in dealing with quotients of integers which may be negative, observe that
```math
\frac{-m}n=\frac m{-n}
```
_Proof._ to test equality, we apply the rule for cross multiplying.
```math
(-m)(-n)=mn
```
```math
\begin{aligned}
&=(-m)(-n)\\
&=-(m(-n))\\
&=-(-(mn))\\
&=mn
\end{aligned}
```
__Theorem 3.__ Any positive rational number has an expression as a fraction in lowest form.

_Proof._ First write a given positive rational number as a quotient of positive integers $`\frac mn`$. We know that 1 is a common divisor of $`m`$ and $`n`$. Any common divisor is at most equal to $`m`$ or $`n`$. We denote $`d`$ as the greatest common divisor. Thus we can write $`m=dr`$ and $`n=ds`$ for some positive integers $`r,s`$.
```math
\frac mn = \frac {dr}{ds}=\frac rs
```
All we have to do now is show that the only common divisor of $`r`$ and $`s`$ is 1. Suppose that $`e`$ is a common divisor which is greater than 1. Then we can write $`r=ex`$ and $`s=ey`$ with positive integers $`x`$ and $`y`$. Hence,
```math
\frac mn = \frac {dr}{ds}=\frac {dex}{dey}
```
Therefore $`de`$ is a common divisor for $`m`$ and $`n`$, and is greater than $`d`$ since $`e`$ is greater than 1. This is impossible because we assumed that $`d`$ was the greatest common divisor of $`m`$ and $`n`$. Therefore 1 is the only common divisor of $`r`$ and $`s`$, and our theorem is proved.

The rule for addition when the rational numbers have a common denominator.
```math
\frac ad + \frac bd = \frac {a+b}d
```
The rule for addition when the rational numbers do not have a common denominator.
```math
\frac mn + \frac rs = \frac {ms+rn}{ns}
```
Namely, let $`\frac mn`$ and $`\frac rs`$ be rational numbers, expressed as quotients of integers $`m,n`$ and $`r,s`$ with $`n \not = 0`$ and $`s \not = 0`$. Then we have seen that $`\frac mn = \frac {sm}{sn}`$ and $`\frac rs = \frac {nr}{ns}`$, thus our rational numbers now have a common denominator $`sn`$.

_The sum of positive rational numbers is also positive._

Observe that our number 0 has the property that $`\frac 0n = \frac 01=0`$ for any integer $`n \not = 0`$. Applying our test for the equality of two fractions, we must verify that $`0\cdot 1=0\cdot n`$, and this is true because both sides are equal to 0.

The forumula for multiplication of rational numbers.
```math
\frac mn \cdot \frac rs = \frac {mr}{ns}
```

Let $`a = m/n`$ be a rational number expressed as a quotient of integers, and $`k`$ be any positive integer. Then
```math
a^k=\left(\frac mn\right)^k=\frac{m^k}{n^k}
```
__Theorem 4.__ There is no positive rational number whose the square is 2.

_Proof._ Suppose that such a rational number exists. We can write it in lowest form $`m/n`$. In particular, not both $`m`$ and $`n`$ can be even.
```math
\left(\frac mn\right)^2=\frac{m^2}{n^2}=2
```
Consequently, we obtain $`m^2=2n^2`$ and therefore $`m^2`$ is even. We conclude that $`m`$ must be even, and we can therefore write $`m=2k`$ for some positive integer $`k`$.
```math
m^2=(2k)^2=4k^2=2n^2
```
We can cancel 2 from both sides of the equation $`4k^2=2n^2`$, and obtain $`n^2=2k^2`$. This means that $`n^2`$ is even, and as before we conclude that $`n`$ itself must be even. We have obtained the impossible fact that both $`m`$ and $`n`$ are even. This means that our original assumption $`(\frac mn)^2=2`$ cannot be true.

A number which is not rational is called irrational. If a positive number $`a`$ exists such that $`a^2 = 2`$, then $`a`$ must be irrational.

__Exercises__
__1.__ Solve for $`a`$ in the following equations.
```math
\begin{aligned}
2a&=\frac 3 4\\
a&=\frac{\frac 3 4}{2}\\
a&=\frac{3}{8}
\end{aligned}
```
```math
\begin{aligned}
\frac{3a}5&=-7\\
a&=-7\cdot \frac 5 3\\
a&=\frac{-35}3
\end{aligned}
```
```math
\begin{aligned}
\frac{-5a}2&=\frac 38\\
a&=\frac 38\cdot -\frac{2}{5}\\
a&=\frac {-1\cdot3}{5\cdot4}\\
a&=\frac {-3}{20}
\end{aligned}
```
__2.__ Solve for $`x`$ in the following equations.
```math
\begin{aligned}
3x-5&=0\\
3x&=5\\
x&=\frac 5 3
\end{aligned}
```
```math
\begin{aligned}
-2x+6&=1\\
-2x&=-5\\
x&=\frac{-5}{-2}\\
x&=\frac{5}{2}
\end{aligned}
```
```math
\begin{aligned}
-7x&=2\\
x&=\frac{2}{-7}\\
x&=\frac{-2}{7}
\end{aligned}
```
__3.__ Put the following fractions in lowest form.
```math
\frac{10}{25}=\frac{2\cdot5}{5\cdot5}=\frac2 5, \enspace \gcd(10,25)=5\newline
```
```math
\frac{3}{9}=\frac{1\cdot3}{3\cdot3}=\frac1 3, \enspace \gcd(3,9)=3
```
```math
\frac{30}{25}=\frac{6\cdot5}{5\cdot5}=\frac6 5, \enspace \gcd(30,25)=5
```
```math
\def\num{50}\def\den{15}\def\fac{5}
\frac{\num}{\den}=\frac{10\cdot \fac}{3\cdot \fac}=\frac{10}3, \enspace \gcd(\num,\den)=\fac
```
```math
\def\num{45}\def\den{9}\def\fac{9}
\frac{\num}{\den}=\frac{5\cdot \fac}{1\cdot \fac}=5, \enspace \gcd(\num,\den)=\fac
```
```math
\def\num{62}\def\den{4}\def\fac{2}
\frac{\num}{\den}=\frac{31\cdot \fac}{2\cdot \fac}=\frac{31}2, \enspace \gcd(\num,\den)=\fac
```
```math
\def\num{23}\def\den{46}\def\fac{23}
\frac{\num}{\den}=\frac{1\cdot \fac}{2\cdot \fac}=\frac{1}2, \enspace \gcd(\num,\den)=\fac
```
```math
\def\num{16}\def\den{40}\def\fac{8}
\frac{\num}{\den}=\frac{2\cdot \fac}{5\cdot \fac}=\frac{2}5, \enspace \gcd(\num,\den)=\fac
```
__4.__ Let $`a=\frac m n`$ be a rational number expressed as a quotient of integers $`m, n`$ with $`m \not= 0`$ and $`n\not= 0`$. Show that there is a rational number $`b`$ such that $`ab=ba=1`$.

Let $`b =\frac n m`$. Then $`ab=\frac m n\cdot \frac nm = 1 `$; $`ba=\frac nm \cdot \frac mn = 1 `$.

__5.__ Solve for $`x`$ in the following equations.
```math
\begin{aligned}
2x-7&=21\\
2x+(7-7)&=7+21\\
2x&=28\\
\frac{2x}{2}&=\frac{28}{2}\\
\frac2 2\cdot x&=\frac{2\cdot14}{2\cdot1}\\
x&=\frac{2}{2}\cdot \frac{14}{1}\\
x&=14
\end{aligned}
```
```math
\begin{aligned}
3(2x-5)&=7\\
\frac{3(2x-5)}{3}&=\frac{7}{3}\\
\frac{3}{3}\cdot (2x-5)&=\frac{7}{3}\\
2x-5&=\frac{7}{3}\\
2x+(5-5)&=\frac{7}{3}+5\\
2x&=\frac{7}{3}+5\\
2x&=\frac{7}{3}+\frac{3\cdot5}{3}\\
2x&=\frac{7+15}{3}\\
2x&=\frac{22}{3}\\
\frac{2x}{2}&=\frac{\frac{22}{2}}{3}\\
\frac{2}{2}\cdot x&=\frac{22}{2\cdot3}\\
x&=\frac{22}{6}\\
x&=\frac{11}{3}\\
\end{aligned}
```
```math
\begin{aligned}
(4x-1)2&=\frac1 4\\
\frac{2(4x-1)}2&=\frac{\frac1 4}2\\
4x-1&=\frac{1}{8}\\
4x+(1-1)&=\frac{1}{8}+1\\
4x&=\frac{1}{8}+\frac{8}{8}\\
4x&=\frac{1+8}{8}\\
4x&=\frac{9}{8}\\
\frac{4x}{4}&=\frac{\frac{9}{4}}{8}\\
x&=\frac{9}{32}
\end{aligned}
```
```math
\begin{aligned}
-4x+3&=5x\\
3+(-4x-5x)&=5x-5x\\
3-9x&=5x-5x\\
3-9x&=0\\
(3-3)-9x&=-3\\
-9x&=-3\\
\frac{-9x}{-9}&=\frac{-3}{-9}\\
x&=\frac{-1}{-3}\\
x&=\frac{1}{3}
\end{aligned}
```
```math
\begin{aligned}
3x-2&=-5x+8\\
5x+3x-2&=(5x-5x)+8\\
8x-2&=8\\
8x+(2-2)&=2+8\\
8x&=10\\
\frac{8x}{8}&=\frac{10}{8}\\
x&=\frac{10}{8}\\
x&=\frac{5}{4}\\
\end{aligned}
```
```math
\begin{aligned}
3x+2&=-3x+4\\
3x+3x+2&=(3x-3x)+4\\
6x+2&=4\\
6x+(2-2)&=4-2\\
6x&=2\\
\frac{6x}6&=\frac{2}6\\
x&=\frac{2}{6}\\
x&=\frac{1}{3}
\end{aligned}
```
```math
\begin{aligned}
\frac{4x}{3}+1&=3x\\
\frac{4x}{3}+\frac{3}{3}&=3x\\
\frac{4x+3}{3}&=3x\\
\frac{3(4x+3)}{3}&=3\cdot3x\\
4x+3&=9x\\
(4x-9x)+3&=9x-9x\\
-5x+3&=0\\
(3-3)-5x&=-3\\
-5x&=-3\\
\frac{-5x}{-5}&=\frac{-3}{-5}\\
x&=\frac{-3}{-5}\\
x&=\frac{3}{5}
\end{aligned}
```
```math
\begin{aligned}
-\frac{3x}2+\frac 4 3&=5x\\
\frac{4}{3}-\frac{3x}{2}&=5x\\
\frac{8}{6}-\frac{9x}{6}&=5x\\
\frac{8-9x}{6}&=5x\\
\frac{6(8-9x)}{6}&=6\cdot5x\\
8-9x&=30x\\
8+(-9x-30x)&=30x-30x\\
8-39x&=0\\
(8-8)-39x&=-8\\
-39x&=-8\\
\frac{-39x}{-39}&=\frac{-8}{-39}\\
x&=\frac{-8}{-39}\\
x&=\frac{8}{39}
\end{aligned}
```
```math
\begin{aligned}
\frac{2x-1}{3}+4x=10\\
\frac{2x-1}{3}+\frac{12x}{3}=10\\
\frac{12x+2x-1}{3}=10\\
\frac{14x-1}{3}=10\\
\frac{3(14x-1)}{3}=3\cdot10\\
14x-1=30\\
14x+(1-1)=30+1\\
14x=31\\
\frac{14x}{14}=\frac{31}{12}\\
x=\frac{31}{12}
\end{aligned}
```
__6.__ Solve for $`x`$ in the following equations.
```math
\begin{aligned}
2x-\frac{3}{7}&=\frac{x}{5}+1\\
\frac{14x}{7}-\frac{3}{7}&=\frac{x}{5}+\frac{5}{5}\\
\frac{14x-3}{7}&=\frac{x+5}{5}\\
\frac{35(14x-3)}{7}&=\frac{35(x+5)}{5}\\
5(14x-3)&=7(x+5)\\
70x-15&=7x+35\\
(70x-7x)-15&=(7x-7x)+35\\
63x-15&=35\\
63x+(15-15)&=15+35\\
63x&=50\\
\frac{63x}{63}&=\frac{50}{63}\\
x&=\frac{50}{63}
\end{aligned}
```
```math
\begin{aligned}
\frac{3}{4}x+5&=-7x\\
\frac{3x}{4}+5&=-7x\\
\frac{3x}{4}+\frac{20}{4}&=-7x\\
\frac{3x+20}{4}&=-7x\\
\frac{4(3x+20)}{4}&=-7\cdot4x\\
3x+20&=-28x\\
28x+3x+20&=28x-28x\\
31x+20&=0\\
31x+(20-20)&=-20\\
31x&=-20\\
x&=\frac{-20}{31}
\end{aligned}
```
```math
\begin{aligned}
\frac{-2}{13}x&=3x-1\\
\frac{-2x}{13}&=3x-1\\
\frac{-2x\cdot 13x}{13}&=13(3x-1)\\
-2x&=39x-13\\
-2x-39x&=(39x-39x)-13\\
-41x&=-13\\
\frac{-41x}{-41}&=\frac{-13}{-41}\\
x&=\frac{-13}{-41}\\
x&=\frac{13}{41}
\end{aligned}
```
```math
\begin{aligned}
\frac{4x}{3}+\frac{3}{4}&=2x-5\\
\frac{16x}{12}+\frac{9}{12}&=2x-5\\
\frac{16x+9}{12}&=2x-5\\
\frac{12(16x+9)}{12}&=12(2x-5)\\
16x+9&=24x-60)\\
(16x-24x)+9&=(24x-24x)-60)\\
9-8x&=-60\\
(9-9)-8x&=-9-60\\
-8x&=-69\\
\frac{-8x}{-8}&=\frac{-69}{-8}\\
x&=\frac{-69}{-8}\\
x&=\frac{69}{8}
\end{aligned}
```
```math
\begin{aligned}
\frac{4(1-3x)}{7}&=2x-1\\
\frac{7\cdot4(1-3x)}{7}&=7(2x-1)\\
4(1-3x)&=14x-7\\
4-12x&=14x-7\\
4+(-12x-14x)&=(14x-14x)-7\\
4-26x&=-7\\
(4-4)-26x&=-4-7\\
-26x&=-11\\
\frac{-26x}{-26}&=\frac{-11}{-26}\\
x&=\frac{-11}{-26}\\
x&=\frac{11}{26}
\end{aligned}
```
```math
\begin{aligned}
\frac{2-x}{3}&=\frac{7}{8}x\\
\frac{2-x}{3}&=\frac{7x}{8}\\
\frac{24(2-x)}{3}&=\frac{24\cdot 7x}{8}\\
8(2-x)&=3\cdot 7x\\
16-8x&=21x\\
16+(-8x-21x)&=21x-21x\\
16-29x&=0\\
(16-16)-29x&=-16\\
-29x&=-16\\
\frac{-29x}{-29}&=\frac{-16}{-29}\\
x&=\frac{-16}{-29}\\
x&=\frac{16}{29}\\
\end{aligned}
```
__7.__ a) Find the value of 5!, 6!, 7!, and 8!
```math
\begin{aligned}
5!&=5\cdot4\cdot3\cdot2\cdot1=120\\
6!&=6\cdot5\cdot4\cdot3\cdot2\cdot1=720\\
7!&=7\cdot6\cdot5\cdot4\cdot3\cdot2\cdot1=5040\\
8!&=8\cdot7\cdot6\cdot5\cdot4\cdot3\cdot2\cdot1=40320
\end{aligned}
```
b) Define 0! = 1. Define the binomial coefficient $`\binom{m}{n}=\frac{m!}{n!(m-n)!}`$ for any natural numbers $`m,n`$ such that $`n`$ lies between 0 and $`m`$. Compute the binomial coefficients.
```math
\begin{aligned}
\def\num{3}
\binom{3}{0}&=\frac{\num!}{0!(\num-0)!}=\frac{6}{1\cdot6}=1\\
\binom{3}{1}&=\frac{\num!}{1!(\num-1)!}=\frac{6}{1\cdot2}=3\\
\binom{3}{2}&=\frac{\num!}{2!(\num-2)!}=\frac{6}{2\cdot1}=3\\
\binom{3}{3}&=\frac{\num!}{3!(\num-3)!}=\frac{6}{6\cdot1}=1\\
\def\num{4}
\binom{4}{0}&=\frac{\num!}{0!(\num-0)!}=\frac{24}{1\cdot24}=1\\
\binom{4}{1}&=\frac{\num!}{1!(\num-1)!}=\frac{24}{1\cdot6}=4\\
\binom{4}{2}&=\frac{\num!}{2!(\num-2)!}=\frac{24}{2\cdot2}=6\\
\binom{4}{3}&=\frac{\num!}{3!(\num-3)!}=\frac{24}{6\cdot1}=4\\
\binom{4}{4}&=\frac{\num!}{4!(\num-4)!}=\frac{24}{24\cdot1}=1\\
\def\num{5}
\binom{5}{0}&=\frac{\num!}{0!(\num-0)!}=\frac{120}{1\cdot120}=1\\
\binom{5}{1}&=\frac{\num!}{1!(\num-1)!}=\frac{120}{1\cdot24}=5\\
\binom{5}{2}&=\frac{\num!}{2!(\num-2)!}=\frac{120}{2\cdot6}=10\\
\binom{5}{3}&=\frac{\num!}{3!(\num-3)!}=\frac{120}{6\cdot2}=10\\
\binom{5}{4}&=\frac{\num!}{4!(\num-4)!}=\frac{120}{24\cdot1}=5\\
\binom{5}{5}&=\frac{\num!}{5!(\num-5)!}=\frac{120}{120\cdot1}=1\\
\end{aligned}
```
c) Show that $`\binom{m}{n}=\binom{m}{m-n}`$.
```math
\begin{aligned}
\binom{m}{m-n}&=\frac{m!}{(m-n)!(m-(m-n))!}\\
    &=\frac{m!}{(m-n)!(m-m+n)!}\\
    &=\frac{m!}{(m-n)!n!}\\
    &=\binom{m}{n}
\end{aligned}
```
d) Show that if $`n`$ is a positive integer at most equal to $`m`$, then $`\binom{m}{n}+\binom{m}{n-1}=\binom{m+1}{n}.`$
```math
\begin{aligned}
\binom{m}{n}+\binom{m}{n-1}&=\frac{m!}{n!(m-n)!}+\frac{m!}{(m-n+1)!(n-1)!}\\
    &=\frac{(m!)(m-n+1)+(m!)n}{n!(m-n+1)!}\\
    &=\frac{m!(m+1)}{n!(m-n+1)!}\\
    &=\frac{(m+1)!}{n!((m+1)-n)!}\\
    &=\binom{m+1}{n}
\end{aligned}
```
__19__.
https://math.stackexchange.com/questions/2086822/having-trouble-with-rational-numbers-problem-serge-lang-basic-mathematics
#### Multiplicative inverses
### Linear Equations
### Real Numbers
### Quadratic Equations
